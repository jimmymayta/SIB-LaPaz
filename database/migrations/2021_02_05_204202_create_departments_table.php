<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('department', 100);
            $table->string('abbreviation', 100);
            $table->integer('state_id');
            $table->integer('personal_id');
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
            $table->foreign('state_id')->references('id')->on('state');
            $table->foreign('personal_id')->references('id')->on('personal');
        });
        DB::table('departments')->insert([
            'department' => 'La Paz',
            'abbreviation' => 'LP',
            'state_id' => DB::table('state')->where('state', 'Activo')->value('id'),
            'personal_id' => DB::table('personal')->where('email', 'jimmy@gmail.com')->value('id'),
            'creationdate' => '2020-01-28 01:39:01'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
