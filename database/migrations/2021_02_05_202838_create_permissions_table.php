<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('permission', 100);
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
        });
        DB::table('permissions')->insert([
            'permission' => 'Lectura',
            'description' => 'Permiso de lectura',
            'detail' => 'Read',
            'creationdate' => '2020-01-28 01:39:01'
        ]);
        DB::table('permissions')->insert([
            'permission' => 'Escritura',
            'description' => 'Permiso de escritura',
            'detail' => 'Write',
            'creationdate' => '2020-01-28 01:39:01'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
