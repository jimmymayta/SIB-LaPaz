<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('state', 100);
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
        });
        DB::table('state')->insert(['state' => 'Activo', 'creationdate' => '2020-01-28 01:39:01']); // 1
        DB::table('state')->insert(['state' => 'Inactivo', 'creationdate' => '2020-01-28 01:39:01']); // 2
        DB::table('state')->insert(['state' => 'Eliminado', 'creationdate' => '2020-01-28 01:39:01']); // 3
        DB::table('state')->insert(['state' => 'Activado', 'creationdate' => '2020-01-28 01:39:01']); // 4
        DB::table('state')->insert(['state' => 'Desactivado', 'creationdate' => '2020-01-28 01:39:01']); // 5
        DB::table('state')->insert(['state' => 'Creacion', 'creationdate' => '2020-01-28 01:39:01']); // 6
        DB::table('state')->insert(['state' => 'Actualizacion', 'creationdate' => '2020-01-28 01:39:01']); // 7
        DB::table('state')->insert(['state' => 'Eliminacion', 'creationdate' => '2020-01-28 01:39:01']); // 8
        DB::table('state')->insert(['state' => 'Inicio', 'creationdate' => '2020-01-28 01:39:01']); // 9
        DB::table('state')->insert(['state' => 'Ingresando', 'creationdate' => '2020-01-28 01:39:01']); // 10
        DB::table('state')->insert(['state' => 'Saliendo', 'creationdate' => '2020-01-28 01:39:01']); // 11
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('state');
    }
}
