<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('group', 100);
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
        });
        DB::table('groups')->insert(['group' => 'Administrador', 'creationdate' => '2020-01-28 01:39:01']);
        DB::table('groups')->insert(['group' => 'Personal', 'creationdate' => '2020-01-28 01:39:01']);
        DB::table('groups')->insert(['group' => 'Asociado', 'creationdate' => '2020-01-28 01:39:01']);
        DB::table('groups')->insert(['group' => 'Curioso', 'creationdate' => '2020-01-28 01:39:01']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
