<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();

            $table->integer('personal_id')->nullable(); // personal Admin (V-Engine)
            $table->integer('id_personal')->nullable(); // personal secundario
            $table->integer('id_state')->nullable(); // state secundario
            $table->integer('id_state_id')->nullable(); // actividad realizada del personal secundario
            $table->string('record', 100)->nullable(); // historial realizada del personal secundario

            $table->integer('state_id')->nullable(); //  default 1
            $table->string('description', 300)->nullable(); // Record
            $table->string('detail', 300)->nullable(); // Record
            $table->dateTime('creationdate')->nullable(); // Date
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();

            $table->foreign('personal_id')->references('id')->on('personal');
            $table->foreign('id_personal')->references('id')->on('personal');
            $table->foreign('id_state')->references('id')->on('state');
            $table->foreign('id_state_id')->references('id')->on('state');
            $table->foreign('state_id')->references('id')->on('state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record');
    }
}
