<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';
            $table->integer('id')->autoIncrement();
            $table->string('code', 100);
            $table->string('names', 100);
            $table->string('firstlastname', 100);
            $table->string('secondlastname', 100)->nullable();
            $table->string('email', 100);
            $table->integer('user_id')->unique();
            $table->integer('group_id');
            $table->integer('permission_id');
            $table->integer('state_id');
            $table->integer('personal_id')->nullable();
            $table->string('description', 300)->nullable();
            $table->string('detail', 300)->nullable();
            $table->dateTime('creationdate')->nullable();
            $table->dateTime('upgradedate')->nullable();
            $table->dateTime('eliminationdate')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('permission_id')->references('id')->on('permissions');
            $table->foreign('state_id')->references('id')->on('state');
            $table->foreign('personal_id')->references('id')->on('personal');
        });
        DB::table('personal')->insert([
            'code' => '20210103010301137890',
            'names' => 'Jimmy',
            'firstlastname' => 'M.',
            'secondlastname' => 'J.',
            'email' => 'jimmy@gmail.com',
            'user_id' => DB::table('users')->where('user', 'jimmy@gmail.com')->value('id'),
            'group_id' => DB::table('groups')->where('group', 'Administrador')->value('id'),
            'permission_id' => DB::table('permissions')->where('permission', 'Escritura')->value('id'),
            'state_id' => DB::table('state')->where('state', 'Activo')->value('id'),
            'creationdate' => '2020-01-28 01:39:01'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal');
    }
}
