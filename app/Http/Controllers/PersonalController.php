<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Personal;
use App\App;

class PersonalController extends Controller
{
    public function personal()
    {
        if (!session('code'))
            return redirect()->route('principal');

        App::record(DB::table('personal')->where('code', session('code'))->value('id'),
        null, 1, 10, 'Personal');

        return view('personal.personal', [
            'user' => DB::table('personal')->where('code', session('code'))->value('names'),
            'group' => DB::table('personal')->where('code', session('code'))->value('group_id'),
            'permission' => DB::table('personal')->where('code', session('code'))->value('permission_id'),
            'code' =>  DB::table('personal')->where('code', session('code'))->value('code'),
            'personal' => DB::table('personal')->where('state_id', 1)->orWhere('state_id', 2)->get()
        ]);
    }

    public function personalDos(Request $request) {

        if (!session('code'))
            return redirect()->route('principal');

        $id =  DB::table('personal')->where('code', session('code'))->value('id');

        $per = Personal::find($request->personal);
        $per->group_id  = $request->group;
        $per->permission_id = $request->permission;
        $per->upgradedate = App::DateTime();
        $per->save();

        App::record($id, $request->personal, 1, 7, 'Personal');

        return redirect()->route('personal');
    }

    public function personalCreate(Request $request)
    {
        if (!session('code'))
            return redirect()->route('principal');

        if (DB::table('users')->where('user', $request->email)->value('user') ||
            DB::table('personal')->where('email', $request->email)->value('email')) {
            return redirect()->route('principal');
        }

        $Use = new User;
        $Use->user = $request->email;
        $Use->password = Hash::make($request->password);
        $Use->creationdate = App::DateTime();
        $Use->save();

        $surnames = explode(' ', $request->surnames);

        $code = App::Code();
        $Per = new Personal;
        $Per->code = $code;
        $Per->names = $request->names;
        $Per->firstlastname = $surnames[0];
        $Per->secondlastname = isset($surnames[1]) ? $surnames[1] : null;
        $Per->email = $request->email;
        $Per->user_id = DB::table('users')->where('user', $request->email)->value('id');
        $Per->group_id = 4;
        $Per->permission_id = 1;
        $Per->state_id = 1;
        $Per->personal_id = DB::table('personal')->where('code', session('code'))->value('id');
        $Per->creationdate = App::DateTime();
        $Per->save();

        App::record(DB::table('personal')->where('code', session('code'))->value('id'),
        DB::table('personal')->where('code', $code)->value('id'), 1, 6, 'Personal');

        return redirect()->route('personal');
    }

    public function personalUpdate(Request $request)
    {
        if (!session('code'))
            return redirect()->route('principal');

        $password = $request->password;

        $Use = User::find($request->user);
        $Use->user = $request->email;
        $Use->password = Hash::make($password);
        $Use->upgradedate = App::DateTime();
        $Use->save();

        $user_id = DB::table('users')->where('user', $request->email)->value('id');
        $personal_id = DB::table('personal')->where('user_id', $user_id)->value('id');

        $surnames = explode(' ', $request->surnames);

        $Per = Personal::find($personal_id);
        $Per->names = $request->names;
        $Per->firstlastname = $surnames[0];
        $Per->secondlastname = isset($surnames[1]) ? $surnames[1] : null;
        $Per->email = $request->email;
        $Per->personal_id = DB::table('personal')->where('code', session('code'))->value('id');
        $Per->upgradedate = App::DateTime();
        $Per->save();

        App::record(DB::table('personal')->where('code', session('code'))->value('id'),
        DB::table('personal')->where('id', $personal_id)->value('id'), 1, 7, 'Personal');

        return redirect()->route('personal');
    }

    public function personalActivate($id_personal) {

        if (!session('code'))
            return redirect()->route('principal');

        $id =  DB::table('personal')->where('code', session('code'))->value('id');

        $per = Personal::find($id_personal);
        $per->state_id = 1;
        $per->personal_id =  $id;
        $per->upgradedate = App::DateTime();
        $per->save();

        App::record($id, $id_personal, 1, 4, 'Personal');

        return redirect()->route('personal');
    }

    public function personalDesactivate($id_personal)
    {
        if (!session('code'))
            return redirect()->route('principal');

        $id =  DB::table('personal')->where('code', session('code'))->value('id');

        $per = Personal::find($id_personal);
        $per->state_id = 2;
        $per->personal_id =  $id;
        $per->upgradedate = App::DateTime();
        $per->save();

        App::record($id, $id_personal, 1, 5, 'Personal');

        return redirect()->route('personal');
    }

    public function personalDelete($personal_id) {
        if (!session('code'))
            return redirect()->route('principal');

        $id =  DB::table('personal')->where('code', session('code'))->value('id');

        $per = Personal::find($personal_id);
        $per->state_id = 3;
        $per->personal_id =  $id;
        $per->eliminationdate = App::DateTime();
        $per->save();

        App::record($id, $personal_id, 3, 8, 'Personal');

        return redirect()->route('personal');
    }
}
