<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\AcademicDegree;
use App\Models\Speciality;
use App\Models\Career;
use App\Models\University;
use App\Models\Department;
use App\Models\Area;
use App\App;

class AdministrationController extends Controller
{
    public function Administration()
    {
        return view('administration.administration', [
            'user' => DB::table('personal')->where('code', session('code'))->value('names'),
            'group' => DB::table('personal')->where('code', session('code'))->value('group_id'),
            'permission' => DB::table('personal')->where('code', session('code'))->value('permission_id'),
            'code' =>  DB::table('personal')->where('code', session('code'))->value('code'),
            'academicdegree' => AcademicDegree::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'speciality' => Speciality::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'career' => Career::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'university' => University::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'department' => Department::where('state_id', 1)->orWhere('state_id', 2)->get(),
            'area' => Area::where('state_id', 1)->orWhere('state_id', 2)->get()
        ]);
    }

    public function academicDegreeCreate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $ad = new AcademicDegree;
        $ad->academicdegree = $request->academicdegree;
        $ad->abbreviation = $request->abbreviation;
        $ad->state_id = 1;
        $ad->personal_id = $id;
        $ad->description = $request->description;
        $ad->creationdate = App::DateTime();
        $ad->save();

        App::record($id, null, 1, 6, 'Academic Degree');

        return redirect()->route('Administration');
    }

    public function academicDegreeUpdate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $ad = AcademicDegree::find($request->academic);
        $ad->academicdegree = $request->academicdegree;
        $ad->abbreviation = $request->abbreviation;
        $ad->state_id = 1;
        $ad->personal_id = $id;
        $ad->description = $request->description;
        $ad->upgradedate = App::DateTime();
        $ad->save();

        App::record($id, null, 1, 7, 'Academic Degree');

        return redirect()->route('Administration');
    }

    public function academicDegreeDelete($academicDegree) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $ad = AcademicDegree::find($academicDegree);
        $ad->state_id = 3;
        $ad->personal_id = $id;
        $ad->Eliminationdate = App::DateTime();
        $ad->save();

        App::record($id, null, 3, 8, 'Academic Degree');

        return redirect()->route('Administration');
    }

    public function specialityCreate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $ad = new Speciality;
        $ad->speciality = $request->speciality;
        $ad->state_id = 1;
        $ad->personal_id = $id;
        $ad->description = $request->description;
        $ad->creationdate = App::DateTime();
        $ad->save();

        App::record($id, null, 1, 6, 'Speciality');

        return redirect()->route('Administration');
    }

    public function specialityUpdate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $spe = Speciality::find($request->specialityUpdate);
        $spe->speciality = $request->speciality;
        $spe->personal_id = $id;
        $spe->description = $request->description;
        $spe->upgradedate = App::DateTime();
        $spe->save();

        App::record($id, null, 1, 7, 'Speciality');

        return redirect()->route('Administration');
    }

    public function specialityDelete($specialityDelete) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $spe = Speciality::find($specialityDelete);
        $spe->state_id = 3;
        $spe->personal_id = $id;
        $spe->eliminationdate = App::DateTime();
        $spe->save();

        App::record($id, null, 1, 8, 'Speciality');

        return redirect()->route('Administration');
    }

    public function careerCreate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $car = new Career;
        $car->career = $request->career;
        $car->state_id = 1;
        $car->personal_id = $id;
        $car->description = $request->description;
        $car->creationdate = App::DateTime();
        $car->save();

        App::record($id, null, 1, 6, 'Career');

        return redirect()->route('Administration');
    }

    public function careerUpdate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $car = Career::find($request->careerUpdate);
        $car->career = $request->career;
        $car->state_id = 1;
        $car->personal_id = $id;
        $car->description = $request->description;
        $car->upgradedate = App::DateTime();
        $car->save();

        App::record($id, null, 1, 7, 'Career');

        return redirect()->route('Administration');
    }

    public function careerDelete($career) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $car = Career::find($career);
        $car->state_id = 3;
        $car->personal_id = $id;
        $car->eliminationdate = App::DateTime();
        $car->save();

        App::record($id, null, 1, 8, 'Career');

        return redirect()->route('Administration');
    }

    public function universityCreate(Request $request) {
        if (!session('code'))
        return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $uni = new University();
        $uni->university = $request->university;
        $uni->state_id = 1;
        $uni->personal_id = $id;
        $uni->description = $request->description;
        $uni->creationdate = App::DateTime();
        $uni->save();

        App::record($id, null, 1, 6, 'University');

        return redirect()->route('Administration');
    }

    public function universityUpdate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $uni = University::find($request->universityUpdate);
        $uni->university = $request->university;
        $uni->state_id = 1;
        $uni->personal_id = $id;
        $uni->description = $request->description;
        $uni->upgradedate = App::DateTime();
        $uni->save();

        App::record($id, null, 1, 7, 'University');

        return redirect()->route('Administration');
    }

    public function universityDelete($university) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $car = University::find($university);
        $car->state_id = 3;
        $car->personal_id = $id;
        $car->eliminationdate = App::DateTime();
        $car->save();

        App::record($id, null, 1, 8, 'University');

        return redirect()->route('Administration');
    }

    public function departmentCreate(Request $request) {
        if (!session('code'))
        return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $dep = new Department();
        $dep->department = $request->department;
        $dep->abbreviation = $request->abbreviation;
        $dep->state_id = 1;
        $dep->personal_id = $id;
        $dep->description = $request->description;
        $dep->creationdate = App::DateTime();
        $dep->save();

        App::record($id, null, 1, 6, 'Department');

        return redirect()->route('Administration');
    }

    public function departmentUpdate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $dep = Department::find($request->departmentUpdate);
        $dep->department = $request->department;
        $dep->abbreviation = $request->abbreviation;
        $dep->state_id = 1;
        $dep->personal_id = $id;
        $dep->description = $request->description;
        $dep->upgradedate = App::DateTime();
        $dep->save();

        App::record($id, null, 1, 7, 'Department');

        return redirect()->route('Administration');
    }

    public function departmentDelete($department) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $car = Department::find($department);
        $car->state_id = 3;
        $car->personal_id = $id;
        $car->eliminationdate = App::DateTime();
        $car->save();

        App::record($id, null, 1, 8, 'Department');

        return redirect()->route('Administration');
    }

    public function areaCreate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $are = new Area();
        $are->area = $request->area;
        $are->state_id = 1;
        $are->personal_id = $id;
        $are->description = $request->description;
        $are->creationdate = App::DateTime();
        $are->save();

        App::record($id, null, 1, 6, 'Area');

        return redirect()->route('Administration');
    }

    public function areaUpdate(Request $request) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $dep = Area::find($request->areaUpdate);
        $dep->area = $request->area;
        $dep->state_id = 1;
        $dep->personal_id = $id;
        $dep->description = $request->description;
        $dep->upgradedate = App::DateTime();
        $dep->save();

        App::record($id, null, 1, 7, 'Area');

        return redirect()->route('Administration');
    }

    public function areaDelete($department) {
        if (!session('code'))
            return redirect()->route('principal');

        $id = DB::table('personal')->where('code', session('code'))->value('id');

        $car = Area::find($department);
        $car->state_id = 3;
        $car->personal_id = $id;
        $car->eliminationdate = App::DateTime();
        $car->save();

        App::record($id, null, 1, 8, 'Area');

        return redirect()->route('Administration');
    }
}
