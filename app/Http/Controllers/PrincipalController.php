<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrincipalController extends Controller
{
    public function principal()
    {
        return view('principal.principal', [
            'user' => DB::table('personal')->where('code', session('code'))->value('names'),
            'group' => DB::table('personal')->where('code', session('code'))->value('group_id'),
            'code' =>  DB::table('personal')->where('code', session('code'))->value('code'),
        ]);
    }
}
