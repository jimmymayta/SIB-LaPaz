<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalInformation extends Model
{
    use HasFactory;
    protected $table = 'personalinformation';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'code',
        'personal_id',
        'CI',
        'department_id',
        'gender_id',
        'cellular',
        'telephone',
        'address',
        'university_id',
        'career_id',
        'speciality_id',
        'academicdegree_id',
        'RU',
        'description',
        'detail',
        'creationdate',
        'upgradedate',
        'eliminationdate'
    ];
}
