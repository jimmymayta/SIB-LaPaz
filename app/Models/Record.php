<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    use HasFactory;
    protected $table = 'record';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'personal_id',
        'id_personal',
        'id_state',
        'id_state_id',
        'record',
        'state_id',
        'description',
        'detail',
        'creationdate',
        'upgradedate',
        'eliminationdate'
    ];
}
