<?php

namespace App;

use Illuminate\Support\Facades\DB;
use App\Models\Record;
use \DateTimeZone;
use \DateTime;
use \DateInterval;

class App
{
    public static function Code()
    {
        $DateTime = new DateTime('now', new DateTimeZone('America/La_Paz'));
        return $DateTime->format('YmdHisu');
    }

    public static function DateTime()
    {
        $DateTime = new DateTime('now', new DateTimeZone('America/La_Paz'));
        return $DateTime->format('Y-m-d H:i:s');
    }

    public static function Session($session)
    {
        if (!session('code')) {
            return redirect()->route('principal');
        }
    }

    public static function record($personal_id, $id_personal, $id_state, $id_state_id, $record) {
        $rec = new Record();
        $rec->personal_id = $personal_id;
        $rec->id_personal = $id_personal;
        $rec->id_state = $id_state;
        $rec->id_state_id = $id_state_id;
        $rec->record = $record;
        $rec->state_id = 1;
        $rec->description = 'Record';
        $rec->detail = 'Record';
        $rec->creationdate = App::DateTime();
        $rec->save();
    }




    public static function Personal($Code)
    {
        $Per = DB::table('Personal')
            ->select('ID_Per', 'Names_Per', 'Surnames_Per', 'Email_Per', 'ID_Gro')
            ->where('Code_Per', '=', $Code)
            ->where('ID_Sta', '=', '1')
            ->get();
        return $Per[0];
    }

    public static function SessionStart($Code)
    {
        session(['Code' => $Code]);
    }

    public static function SessionEnd()
    {
        session()->forget('Code');
    }

    public static function Security()
    {
        if (session('Code')) {
            if ($Code = DB::table('Personal')
                ->where('Code_Per', session('Code'))
                ->value('Code_Per')
            ) {
                $Per = DB::table('Personal')
                    ->select(
                        'ID_Per',
                        'Names_Per',
                        'Surnames_Per',
                        'Email_Per',
                        'ID_Mod',
                        'ID_Gro'
                    )
                    ->where('Code_Per', $Code)
                    ->where('ID_Sta', '1')
                    ->get();
                return $Per[0];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function CountDate($Date, $Number)
    {
    }



    public static function DateTime2($Date)
    {
        $DateTime = new DateTime($Date, new DateTimeZone('America/La_Paz'));
        return $DateTime->format('Y-m-d H:i:s');
    }

    public static function DateDifference($Date)
    {
        $Date1 = new DateTime($Date, new DateTimeZone('America/La_Paz'));
        $Date2 = new DateTime('now', new DateTimeZone('America/La_Paz'));
        $Diff = $Date1->diff($Date2);
        if ($Date1->format('Y-m-d') > $Date2->format('Y-m-d')) {
            return $Diff->days * -1;
        } else {
            return $Diff->days;
        }
    }

    public static function DateDifference2($Dat1, $Dat2)
    {
        $Date1 = new DateTime($Dat1, new DateTimeZone('America/La_Paz'));
        $Date2 = new DateTime($Dat2, new DateTimeZone('America/La_Paz'));
        $Diff = $Date1->diff($Date2);
        if ($Date1->format('Y-m-d') > $Date2->format('Y-m-d')) {
            return $Diff->days * -1;
        } else {
            return $Diff->days;
        }
    }

    public static function DateDifference3($Dat1, $Dat2)
    {
        $Date1 = new DateTime($Dat1, new DateTimeZone('America/La_Paz'));
        $Date2 = new DateTime($Dat2, new DateTimeZone('America/La_Paz'));
        $Diff = $Date1->diff($Date2);
        return $Diff->days;
    }

    public static function AddDay($Date, $Number)
    {
        $DateTimeZone = new DateTimeZone('America/La_Paz');
        $DateTime = new DateTime($Date, $DateTimeZone);
        $DateInterval = new DateInterval('P' . $Number . 'D');
        $DateTime->add($DateInterval);
        return $DateTime->format('Y-m-d');
    }
}
