# **S. I. B. - La Paz**
**Sistema inteligente de control de cuotas.**  
Es el primer sistema que tiene el motor de analisis de datos (V-Engine).  
V-Engine esta en constante analisis del sistema y de sus datos.

**Proyecto open source.**

## Sistema Operativo

- GNU/Linux (Ubuntu 20.04.1 LTS)

## Requerimientos

- PHP 7.3.19
- MariaDB 10.3.25
- Composer 2.0.8
- Node.JS

## Framework

- Laravel Framework (Version 8.25.0)
- Material Design for Bootstrap (MDB UI KIT Free 3.0.0)

## Instalación

Para descargar o clonar el proyecto:
<https://gitlab.com/JimmyMayta/SIB-LaPaz.git>

    jimmy@JM:~/Escritorio$ git clone https://gitlab.com/JimmyMayta/SIB-LaPaz.git

Creando la Base de datos

    MariaDB [(none)]> CREATE USER 'SIBLaPaz'@'localhost' IDENTIFIED BY '9283741263';
    MariaDB [(none)]> CREATE DATABASE SIBLaPaz_Database CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';
    MariaDB [(none)]> GRANT ALL PRIVILEGES ON SIBLaPaz_Database.* TO 'SIBLaPaz'@'localhost';
    MariaDB [(none)]> GRANT GRANT OPTION ON SIBLaPaz_Database.* TO 'SIBLaPaz'@'localhost';
    MariaDB [(none)]> FLUSH PRIVILEGES;

Instalar modulos

    jimmy@JM:~/Escritorio/SIB-LaPaz$ npm install

Aplicando Migraciones

    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan migrate

Desplegar proyecto

    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan serve

## Restricciones
1. Desarrollado solo para Bolivia
2. Unico idioma español
3. Solo controla cuotas
4. Moneda (Bs)

## Ayuda extra

Limpiar proyecto

    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan config:clear
    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan config:cache
    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan cache:clear
    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan route:cache
    jimmy@JM:~/Escritorio/SIB-LaPaz$ php artisan route:clear

Recompilar

    jimmy@JM:~/Escritorio/SIB-LaPaz$ npm rebuild

## Recientes actualizaciones importantes

| Fecha | Descripción |
|-------|-------------|
| 09/01/2021 | Inicio de proyecto |
| 03/02/2021 | Eliminación de Material Design for Bootstrap (MDB UI KIT Free 3.0.0) |
| 05/02/2021 | Actualización de proyecto |
| 13/02/2021 | Proyecto finalizado (SIB-LaPaz) |

***
![Imagen](20210127164421.png "jimmyymaytaj@gmail.com")

![Imagen](SIB-LaPaz_Database.png "jimmyymaytaj@gmail.com")

    (base) jimmy@JM:~/ProjectsPHP/SIB-LaPaz$ 
***
