<nav class="navbar sticky-top navbar-expand-sm navbar-dark bg-primary mb-3">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ route('principal') }}">SIB - La Paz</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
    aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-0 mb-sm-0">
        @if ($group === 1 || $group === 2)
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="" id="navbarDropdown"
            role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Principal
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="dropdown-item" href="{{ route('principal') }}">Principal</a></li>
              <li><a class="dropdown-item" href="{{ route('personal') }}">Personal</a></li>
              <li><a class="dropdown-item" href="{{ route('Administration') }}">Administración</a></li>
              {{-- <li><a class="dropdown-item" href="">Aplicación</a></li> --}}
            </ul>
          </li>
        @else
          <li class="nav-item">
            <a class="nav-link text-white active" aria-current="page" href="">Principal</a>
          </li>
        @endif

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="" id="navbarDropdown"
          role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Nosotros
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="">Visión</a></li>
            <li><a class="dropdown-item" href="">Misión</a></li>
            <li><a class="dropdown-item" href="">Nosotros</a></li>
            <li><a class="dropdown-item" href="">Información</a></li>
          </ul>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="" id="navbarDropdown"
          role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Contacto
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="">Contacto</a></li>
            <li><a class="dropdown-item" href="">Dirección</a></li>
          </ul>
        </li>
      </ul>

      <ul class="navbar-nav ms-auto mb-0 mb-sm-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle active" href="" id="navbarDropdown"
          role="button" data-bs-toggle="dropdown" aria-expanded="false">
            @if ($user)
              {{ $user }}
            @else
              <i class="bi bi-person-fill" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
            @endif
          </a>

          <ul class="dropdown-menu dropdown-menu-sm-end" aria-labelledby="navbarDropdown">
            @if ($user)
              <li><a class="dropdown-item" href="{{ route('personalAccount', $code ) }}">Cuenta personal</a></li>
              <li><a class="dropdown-item" href="{{ route('logout') }}">Salir</a></li>
            @else
              <li><a class="dropdown-item" href="" data-bs-toggle="modal" data-bs-target="#loginModal">Login</a></li>
              <li><a class="dropdown-item" href="" data-bs-toggle="modal" data-bs-target="#personalCreateModal">Registrarse</a></li>
            @endif
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

@include('login.login')
@include('personal.personalcreate')
