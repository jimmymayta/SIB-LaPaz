<div class="modal fade" id="personalUpdateModal{{ $per->id }}" tabindex="-1"
aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Actualizar (N. {{ $per->id }})</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('personalUpdate') }}" method="post">
          @csrf
          <input type="hidden" name="user" value="{{ $per->user_id }}">
          <div class="row g-3 mb-3">
            <div class="col-12 col-sm-12 col-lg-6">
              <input name="names" type="text" class="form-control" value="{{ $per->names }}" placeholder="Nombres" aria-label="Nombres" required>
            </div>

            <div class="col-12 col-sm-12 col-lg-6">
              <input name="surnames" type="text" class="form-control" value="{{ $per->firstlastname }} {{ $per->secondlastname }}" placeholder="Apellidos" aria-label="Apellidos" required>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <div class="col-12 col-sm-12 col-lg-12">
              <input name="email" type="email" class="form-control" value="{{ $per->email }}" placeholder="Correo electrónico"
              aria-label="Correo electrónico" required>
            </div>
          </div>

          <div class="row g-3 mb-3">
            <div class="col-12 col-sm-12 col-lg-6">
              <input id="password1" name="password" type="password" class="form-control" placeholder="Contraseña"
              aria-label="Contraseña" required>
            </div>

            <div class="col-12 col-sm-12 col-lg-6">
              <input id="password2" name="password_confirm" type="password" class="form-control" placeholder="Confirmar contraseña"
              aria-label="Confirmar contraseña" required>
            </div>
          </div>

          <button type="submit" class="btn btn-primary">Registrarse</button>
        </form>
      </div>
    </div>
  </div>
</div>
