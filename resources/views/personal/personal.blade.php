@extends('layouts.app')
@section('content')
  <h2 class="mb-4 text-white">Personal</h2>

  @if ($permission == 2)
    <button type="button" class="btn btn-success mb-3"
      data-bs-toggle="modal" data-bs-target="#personalCreateModal">
      <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
    </button>
  @endif

  <div class="table-responsive mb-3">
    <table id="TableDataTables" class="cell-border compact hover" style="width:100%">
      <thead>
        <tr>
            <th class="text-white">N.</th>
            <th class="text-white">Nombres</th>
            <th class="text-white">A. Paterno</th>
            <th class="text-white">A. Materno</th>
            <th class="text-white">Email</th>
            <th class="text-white text-center"></th>
            @if ($permission == 2)
              <th class="text-white text-center"></th>
              <th class="text-white text-center"></th>
              <th class="text-white text-center"></th>
              <th class="text-white text-center"></th>
            @endif
        </tr>
      </thead>
      <tbody>
          @foreach ($personal as $per)
            <tr>
              <td>{{ $per->id }}</td>
              <td class=" @if ($per->state_id === 2) text-decoration-line-through @endif ">{{ $per->names }}</td>
              <td>{{ $per->firstlastname }}</td>
              <td>{{ $per->secondlastname }}</td>
              <td>{{ $per->email }}</td>
              <td class="text-center">
                <a href="{{ route('personalAccount', $per->code ) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top"
                  title="Ver la cuenta de {{ $per->names }}">
                  <i class="bi bi-eye-fill"></i>
                </a>
              </td>

              @if ($permission == 2)
                <td class="text-center">
                  <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#personal2Modal{{ $per->id }}">
                    <i class="bi bi-plus-circle-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Mas opciones"></i>
                  </a>
                </td>

                <td class="text-center">
                  <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#personalUpdateModal{{ $per->id }}">
                    <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                  </a>
                </td>

                <td class="text-center">
                  @if ($per->state_id === 1)
                    <a href="{{ route('personalDesactivate', $per->id) }}" class="text-dark">
                      <i class="bi bi-toggle-off" data-bs-toggle="tooltip" data-bs-placement="top" title="Desactivar"></i>
                    </a>
                  @else
                    <a href="{{ route('personalActivate', $per->id) }}"  class="text-dark">
                      <i class="bi bi-toggle-on" data-bs-toggle="tooltip" data-bs-placement="top" title="Activar"></i>
                    </a>
                  @endif
                </td>

                <td class="text-center">
                  <a href="{{ route('personalDelete', $per->id) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                    <i class="bi bi-trash-fill"></i>
                  </a>
                </td>
              @endif
            </tr>
            @include('personal.personal2')
            @include('personal.personalupdate')
          @endforeach
      </tbody>
    </table>
  </div>

@endsection

