<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Login</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('login') }}" method="post">
          @csrf
          <div class="input-group mb-3">
            <span id="basic-addon1" class="input-group-text" >
              <i class="bi bi-person-fill" style="font-size: 1rem; color: rgb(0, 0, 0);"></i>
            </span>
            <input type="email" class="form-control" name="email"
            aria-label="Correo" aria-describedby="basic-addon1" placeholder="Correo electrónico">
          </div>
          <div class="input-group mb-3">
            <span id="basic-addon2" class="input-group-text">
              <i class="bi bi-key-fill" style="font-size: 1rem; color: rgb(0, 0, 0);"></i>
            </span>
            <input type="password" class="form-control" name="password"
            aria-label="Contrasena" aria-describedby="basic-addon2" placeholder="Contraseña">
          </div>

          <button type="submit" class="btn btn-primary">Login</button>
        </form>
      </div>
    </div>
  </div>
</div>
