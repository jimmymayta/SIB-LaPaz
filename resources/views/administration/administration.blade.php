@extends('layouts.app')
@section('content')
  <h1 class="text-white">Administración</h1>
  <div class="accordion bg-white" id="accordionExample">
    @include('administration.academicdegree.academicdegree')
    @include('administration.speciality.speciality')
    @include('administration.career.career')
    @include('administration.university.university')
    @include('administration.department.department')
    {{-- @include('administration.area.area') --}}
  </div>
@endsection
