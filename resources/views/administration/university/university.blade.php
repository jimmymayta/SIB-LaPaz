<div class="accordion-item">
  <h2 class="accordion-header" id="headingFour">
    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
    data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
      Universidad
    </button>
  </h2>
  <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
  data-bs-parent="#accordionExample">
    <div class="accordion-body">
      @if ($permission == 2)
      <button type="button" class="btn btn-success btn-sm mb-1"
        data-bs-toggle="modal" data-bs-target="#universityModal">
        <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
      </button>
      @endif
      <div class="table-responsive">
        <table class="table align-middle table-sm">
          <thead>
            <tr>
              <td>N.</td>
              <td>Carera</td>
              <td>Descripción</td>
              @if ($permission == 2)
              <td></td>
              <td></td>
              @endif
            </tr>
          </thead>
          <tbody>
            @foreach ($university as $uni)
              <tr>
                <td>{{ $uni->id }}</td>
                <td>{{ $uni->university }}</td>
                <td>{{ $uni->description }}</td>
                @if ($permission == 2)
                <td>
                  <a href="" class="text-dark" data-bs-toggle="modal" data-bs-target="#universityUpdateModal{{ $uni->id }}">
                    <i class="bi bi-pencil-fill" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar"></i>
                  </a>
                </td>
                <td>
                  <a href="{{ route('universityDelete', $uni->id) }}" class="text-dark" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                    <i class="bi bi-trash-fill"></i>
                  </a>
                </td>
                @endif
              </tr>
              @include('administration.university.universityupdate')
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('administration.university.universitycreate')
