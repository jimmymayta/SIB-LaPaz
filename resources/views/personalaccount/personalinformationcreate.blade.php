<div class="modal fade" id="personalinformationCreateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informacion personal</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{ route('personalInformationCreate') }}" method="post">
          @csrf
          <input type="hidden" name="code" value="{{ $code }}">
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">CI</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="ci" placeholder="CI" required>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">RU</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="ru" placeholder="CI" required>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Fecha titutlo</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" name="fechatitulo" placeholder="Fecha" required>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">departamento</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="department" required>
                <option></option>
                @foreach ($department as $dep)
                  <option value="{{ $dep->id }}">{{ $dep->department }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Genero</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="gender" required>
                <option></option>
                @foreach ($gender as $gen)
                  <option value="{{ $gen->id }}">{{ $gen->gender }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Celular</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="cellular" placeholder="Celular" required>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Telefono</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="telephone" placeholder="Telefono" required>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Direccion</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="address" placeholder="Direccion" required>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Universidad</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="university" required>
                <option></option>
                @foreach ($university as $uni)
                  <option value="{{ $uni->id }}">{{ $uni->university }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Carrera</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="career" required>
                <option></option>
                @foreach ($career as $car)
                  <option value="{{ $car->id }}">{{ $car->career }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Especialidad</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="speciality" required>
                <option></option>
                @foreach ($speciality as $car)
                  <option value="{{ $car->id }}">{{ $car->speciality }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Grado academico</label>
            <div class="col-sm-10">
              <select class="form-select" aria-label="Default select" name="academicdegree" required>
                <option></option>
                @foreach ($academicdegree as $car)
                  <option value="{{ $car->id }}">{{ $car->academicdegree }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="mb-3">
            <textarea id="TextareaDescription" class="form-control"
            name="description" placeholder="Descripción"></textarea>
          </div>

          <button type="submit" class="btn btn-primary btn-sm" >Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>
