@extends('layouts.app')
@section('content')
  <ul class="nav nav-tabs bg-white" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
      <a class="nav-link active" id="home-tab" data-bs-toggle="tab" href="#home" role="tab"
        aria-controls="home" aria-selected="true">Datos</a>
    </li>
    <li class="nav-item" role="presentation">
      <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab"
        aria-controls="profile" aria-selected="false">Informacion personal</a>
    </li>
    {{-- <li class="nav-item" role="presentation">
      <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#contact" role="tab"
        aria-controls="contact" aria-selected="false">Cuotas</a>
    </li> --}}
  </ul>
  <div class="tab-content bg-white mb-3" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <td class="fw-bold">Nombres</td>
              <td class="fw-bold">A. Paterno</td>
              <td class="fw-bold">A. Materno</td>
            </tr>
            <tr>
              <td>{{ $personal->names }}</td>
              <td>{{ $personal->firstlastname }}</td>
              <td>{{ $personal->secondlastname }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
      @if ($personalinformation)
        <div class="table-responsive">
          <table class="table">
            <tbody>
              <tr>
                <td class="fw-bold">CI</td>
                <td class="fw-bold">celular</td>
                <td class="fw-bold">Telefono</td>
                <td class="fw-bold">Direccion</td>
                <td class="fw-bold">ru</td>
              </tr>
              <tr>
                <td>{{ $personalinformation->ci }}</td>
                <td>{{ $personalinformation->cellular }}</td>
                <td>{{ $personalinformation->telephone }}</td>
                <td>{{ $personalinformation->address }}</td>
                <td>{{ $personalinformation->ru }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      @else
        @if ($permission == 2)
          <button type="button" class="btn btn-success btn-sm m-1"
            data-bs-toggle="modal" data-bs-target="#personalinformationCreateModal">
            <i class="bi bi-plus" style="font-size: 1rem; color: rgb(255, 255, 255);"></i>
          </button>
        @endif
      @endif
      @include('personalaccount.personalinformationcreate')
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">

    </div>
  </div>

  <div class="table-responsive mb-3">
    <table id="TableDataTables" class="cell-border compact hover" style="width:100%">
      <thead>
        <tr>
            <th class="text-white">N.</th>
            <th class="text-white">fee</th>
            <th class="text-white">fee 2</th>
            <th class="text-white">fee 3</th>
            <th class="text-white">fee 4</th>
            @if ($permission == 2)
              <th class="text-white">fee 5</th>
            @endif
        </tr>
      </thead>
      <tbody>
        @foreach ($fee as $fee)
          <tr>
            <th>{{ $fee->fee }}</th>
            <th>35 Bs</th>
            <th>{{ $fee->fee2 }}</th>
            <th>{{ $fee->fee3 }}</th>
            <th>{{ $fee->upgradedate }}</th>
            @if ($permission == 2)
              <th>
                @if ($fee->fee5 == 1)
                  <a href="{{ route('nofee', [$code, $fee->id, $fee->fee]) }}"><i class="bi bi-check-square-fill"></i></a>
                @else
                  <a href="{{ route('fee', [$code, $fee->id, $fee->fee]) }}" class="text-dark">
                    <i class="bi bi-app"></i>
                  </a>
                @endif
              </th>
            @endif
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@endsection




















