<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PrincipalController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdministrationController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\PersonalAccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PrincipalController::class, 'principal'])->name('principal');
Route::get('/vision', [PrincipalController::class, 'vision'])->name('vision');
Route::get('/mision', [PrincipalController::class, 'mision'])->name('mision');
Route::get('/nosotros', [PrincipalController::class, 'nosotros'])->name('nosotros');
Route::get('/informacion', [PrincipalController::class, 'informacion'])->name('informacion');
Route::get('/contacto', [PrincipalController::class, 'contacto'])->name('contacto');
Route::get('/direccion', [PrincipalController::class, 'direccion'])->name('direccion');

Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/cuenta/{code}', [PersonalAccountController::class, 'personalAccount'])->name('personalAccount');
Route::post('/personal-information-create', [PersonalAccountController::class, 'personalInformationCreate'])->name('personalInformationCreate');
Route::get('/fee/{code}/{id}/{fee}', [PersonalAccountController::class, 'fee'])->name('fee');
Route::get('/no-fee/{code}/{id}/{fee}', [PersonalAccountController::class, 'nofee'])->name('nofee');

Route::get('/personal', [PersonalController::class, 'personal'])->name('personal');
Route::post('/personaldos', [PersonalController::class, 'personalDos'])->name('personalDos');
Route::post('/personal-create', [PersonalController::class, 'personalCreate'])->name('personalCreate');
Route::post('/personal-update', [PersonalController::class, 'personalUpdate'])->name('personalUpdate');
Route::get('/personal-activate/{id}', [PersonalController::class, 'personalActivate'])->name('personalActivate');
Route::get('/personal-desactivate/{id}', [PersonalController::class, 'personalDesactivate'])->name('personalDesactivate');
Route::get('/personal-delete/{id}', [PersonalController::class, 'personalDelete'])->name('personalDelete');

Route::get('/administracion', [AdministrationController::class, 'Administration'])->name('Administration');
Route::post('/academicdegree-create', [AdministrationController::class, 'academicDegreeCreate'])->name('academicDegreeCreate');
Route::post('/academicdegree-update', [AdministrationController::class, 'academicDegreeUpdate'])->name('academicDegreeUpdate');
Route::get('/academicdegree-delete/{academicdegree}', [AdministrationController::class, 'academicDegreeDelete'])->name('academicDegreeDelete');

Route::post('/speciality-create', [AdministrationController::class, 'specialityCreate'])->name('specialityCreate');
Route::post('/speciality-update', [AdministrationController::class, 'specialityUpdate'])->name('specialityUpdate');
Route::get('/speciality-delete/{id}', [AdministrationController::class, 'specialityDelete'])->name('specialityDelete');

Route::post('/career-create', [AdministrationController::class, 'careerCreate'])->name('careerCreate');
Route::post('/career-update', [AdministrationController::class, 'careerUpdate'])->name('careerUpdate');
Route::get('/career-delete/{id}', [AdministrationController::class, 'careerDelete'])->name('careerDelete');

Route::post('/university-create', [AdministrationController::class, 'universityCreate'])->name('universityCreate');
Route::post('/university-update', [AdministrationController::class, 'universityUpdate'])->name('universityUpdate');
Route::get('/university-delete/{id}', [AdministrationController::class, 'universityDelete'])->name('universityDelete');

Route::post('/department-create', [AdministrationController::class, 'departmentCreate'])->name('departmentCreate');
Route::post('/department-update', [AdministrationController::class, 'departmentUpdate'])->name('departmentUpdate');
Route::get('/department-delete/{id}', [AdministrationController::class, 'departmentDelete'])->name('departmentDelete');

Route::post('/area-create', [AdministrationController::class, 'areaCreate'])->name('areaCreate');
Route::post('/area-update', [AdministrationController::class, 'areaUpdate'])->name('areaUpdate');
Route::get('/area-delete/{id}', [AdministrationController::class, 'areaDelete'])->name('areaDelete');














